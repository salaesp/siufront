'use strict';

angular.module('services.config', [])
  .constant('configuration', {
    siubackurl: '@@siubackurl'
  });
