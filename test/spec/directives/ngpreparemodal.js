'use strict';

describe('Directive: ngPrepareModal', function () {

  // load the directive's module
  beforeEach(module('siufrontApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<ng-prepare-modal></ng-prepare-modal>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the ngPrepareModal directive');
  }));
});
