'use strict';

describe('Service: acHistoryService', function () {

  // load the service's module
  beforeEach(module('siufrontApp'));

  // instantiate service
  var acHistoryService;
  beforeEach(inject(function (_acHistoryService_) {
    acHistoryService = _acHistoryService_;
  }));

  it('should do something', function () {
    expect(!!acHistoryService).toBe(true);
  });

});
