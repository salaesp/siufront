'use strict';

describe('Service: examservice', function () {

  // load the service's module
  beforeEach(module('siufrontApp'));

  // instantiate service
  var examservice;
  beforeEach(inject(function (_examservice_) {
    examservice = _examservice_;
  }));

  it('should do something', function () {
    expect(!!examservice).toBe(true);
  });

});
