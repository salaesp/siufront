'use strict';

describe('Controller: SubinscriptionCtrl', function () {

  // load the controller's module
  beforeEach(module('siufrontApp'));

  var SubinscriptionCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SubinscriptionCtrl = $controller('SubinscriptionCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
