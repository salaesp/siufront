'use strict';

describe('Controller: ShowinscriptionsCtrl', function () {

  // load the controller's module
  beforeEach(module('siufrontApp'));

  var ShowinscriptionsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ShowinscriptionsCtrl = $controller('ShowinscriptionsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
