'use strict';

describe('Controller: GeolocCtrl', function () {

  // load the controller's module
  beforeEach(module('siufrontApp'));

  var GeolocCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    GeolocCtrl = $controller('GeolocCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
