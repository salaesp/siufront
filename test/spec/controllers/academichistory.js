'use strict';

describe('Controller: AcademichistoryCtrl', function () {

  // load the controller's module
  beforeEach(module('siufrontApp'));

  var AcademichistoryCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AcademichistoryCtrl = $controller('AcademichistoryCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
