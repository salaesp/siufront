'use strict';

/**
 * @ngdoc overview
 * @name siufrontApp
 * @description
 * # siufrontApp
 *
 * Main module of the application.
 */
 angular
 .module('siufrontApp', [
  'ngAnimate',
  'ngCookies',
  'ngResource',
  'ngRoute',
  'ngSanitize',
  'ngTouch',
  'restangular',
  'base64',
  'services.config',
  'uiGmapgoogle-maps'
  ])
 .config(function ($routeProvider) {
  $routeProvider
  .when('/', {
    templateUrl: 'views/login.html',
    controller: 'LoginCtrl'
  })
  .when('/login', {
    templateUrl: 'views/login.html',
    controller: 'LoginCtrl'
  })
  .when('/achistory',{
    templateUrl: 'views/achistory.html',
    controller: 'AcademichistoryCtrl'
  })
  .when('/careers', {
    templateUrl: 'views/careers.html',
    controller: 'CareersCtrl'
  })
  .when('/inscriptions', {
    templateUrl: 'views/showinscriptions.html',
    controller: 'ShowinscriptionsCtrl'
  })
  .when('/subinscription', {
    templateUrl: 'views/subinscription.html',
    controller: 'SubinscriptionCtrl'
  })
  .when('/examinscription', {
    templateUrl: 'views/examinscription.html',
    controller: 'ExaminscriptionCtrl'
  })
  .when('/geoloc', {
    templateUrl: 'views/geoloc.html',
    controller: 'GeolocCtrl'
  })
  .when('/logout', {
    templateUrl: 'views/logout.html',
    controller: 'LogoutCtrl'
  });
});
