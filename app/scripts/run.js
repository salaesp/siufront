'use strict';

angular.module('siufrontApp').run(['Restangular', '$window', '$rootScope', '$location', '$route', function(Restangular, $window, $rootScope, $location, $route){

  $rootScope.$on('$routeChangeStart',function(event, next, current){
    if ($rootScope.userLoginKey === null) {
      if ( next.templateUrl === 'views/login.html') {
      } else {
        $location.path('/login');
      }
    }
  });

  var pendingRequests = 0;

  Restangular.setRequestInterceptor(
    function (element, operation, what, url) {
      if (pendingRequests === 0) {
        $rootScope.loading=true;
      }
      pendingRequests++;
      return element;
    });

  Restangular.setResponseInterceptor(
    function (data, operation, what, url, response, deferred) {
      pendingRequests--;
      if (pendingRequests === 0) {
        $rootScope.loading=false;
      }
      return data;
    });

  Restangular.setErrorInterceptor(
    function(response, deferred, responseHandler) {
      pendingRequests--;
      if (pendingRequests === 0) {
        $rootScope.loading=false;
      }
      if(response.status === 401 || response.status === 400){
        $rootScope.selectedAcademicUnit = null;
        $rootScope.selectedCareerPlan = null;
        $rootScope.selectedCareer = null;
        $rootScope.userLoginKey = null;
        $location.path('/login');
        return true;
      }
      Materialize.toast(response.data.message, 4000);
      $route.reload();
      return true; // error not handled
    });
}]);