'use strict';

/**
 * @ngdoc function
 * @name siufrontApp.controller:SubinscriptionCtrl
 * @description
 * # SubinscriptionCtrl
 * Controller of the siufrontApp
 */
 angular.module('siufrontApp')
 .controller('SubinscriptionCtrl', function ($scope, subjectService, $route) {
 	var subjectsPromise = subjectService.getSubjectsInformation();

 	subjectsPromise.then(function(result) {
 		$scope.availableSubjects = result.availableSubjects;
 	}, function error(reason) {
 		console.log(reason.data.message);
 	});

 	$scope.addInscription = function(subjectId, variant) {
 		var subjectsPromise = subjectService.addInscription(subjectId, variant);

 		subjectsPromise.then(function(result) {
 			Materialize.toast('Inscripción creada', 4000);
 			$route.reload();
 		}, function error(reason) {
 			console.log(reason.data.message);
 		});
 	};

 });
