'use strict';

/**
 * @ngdoc function
 * @name siufrontApp.controller:CareersCtrl
 * @description
 * # CareersCtrl
 * Controller of the siufrontApp
 */
 angular.module('siufrontApp')
 .controller('CareersCtrl', function ($rootScope, $scope, $location, acHistoryService) {
 	var careersPromise = acHistoryService.getUserCareers();
 	careersPromise.then(function(result) {
 		$scope.careerInscriptions = result.careers;
 	}, function error(reason) {
 		console.log(reason.data.message);
 	});

 	$scope.selectCareer = function(academic,career, plan) {
 		$rootScope.selectedAcademicUnit = academic;
 		$rootScope.selectedCareerPlan = plan;
 		$rootScope.selectedCareer = career;
 		Materialize.toast('Carrera Seleccionada', 4000);
 		$location.path('/achistory');
 	};
 });
