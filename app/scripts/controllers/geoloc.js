'use strict';

/**
 * @ngdoc function
 * @name siufrontApp.controller:GeolocCtrl
 * @description
 * # GeolocCtrl
 * Controller of the siufrontApp
 */
 angular.module('siufrontApp')
 .controller('GeolocCtrl', function ($scope, $rootScope, geoService) {

 	function onSuccess(position){
 		var near = geoService.putUserCoordinates(position.coords.latitude,
 			position.coords.longitude);

 		$scope.map = { 
 			center: { 
 				latitude: position.coords.latitude,
 				longitude: position.coords.longitude
 			},
 			zoom: 14,
 			options:{
 				streetViewControl :false,
 				mapTypeControl: false
 			}
 		};
 		$scope.meonmap = {
 			coords: position.coords,
 			id: 'me'
 		};

 		near.then(function(result) {
 			var data = [];
 			angular.forEach(result, function(value, key) {
 				var obj = {
 					coords: value.coordinate,
 					id: value.student.id,
 					name: value.student.name,
 					status: value.status
 				};
 				data.push(obj);
 			});
 			$scope.nearme = data;
 		});
 	}

 	function onError(error){
 		Materialize.toast('No se pudo obtener la localización', 4000);
 		$scope.locationdisabled=true;
 	}

 	var options = { timeout: 7000, enableHighAccuracy: true, maximumAge: 90000000 };

 	if (navigator.geolocation) {
 		navigator.geolocation.getCurrentPosition(onSuccess, onError, options);
 	}

 	$scope.putCoordinates = function(user) {
 		if (navigator.geolocation) {
 			navigator.geolocation.getCurrentPosition(function(position){
 				var near = geoService.putUserCoordinatesWithStatus(position.coords.latitude, position.coords.longitude, user.status);
 				near.then(function(result) {
 					Materialize.toast('Estado actualizado', 4000);
 				});
 			});
 		}
 	};
 });
