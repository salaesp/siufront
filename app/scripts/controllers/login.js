'use strict';

/**
 * @ngdoc function
 * @name siufrontApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the siufrontApp
 */
 angular.module('siufrontApp')
 .controller('LoginCtrl', function ($rootScope, $scope, $location, LoginService) {
 	$scope.login = function(login) {
 		var userLoginPromise = LoginService.restLogin(login);
 		userLoginPromise.then(function(result) {
 			$rootScope.userLoginKey = result.loginKey;
 			$location.path('/careers');
 		}, function error(reason) {
 			Materialize.toast('Usuario o contraseña incorrectos', 4000);
 		});
 	};
 });
