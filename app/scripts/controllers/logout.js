'use strict';

/**
 * @ngdoc function
 * @name siufrontApp.controller:LogoutCtrl
 * @description
 * # LogoutCtrl
 * Controller of the siufrontApp
 */
 angular.module('siufrontApp')
 .controller('LogoutCtrl', function ($rootScope, $location) {
 	$rootScope.selectedAcademicUnit = null;
 	$rootScope.selectedCareerPlan = null;
 	$rootScope.selectedCareer = null;
 	$rootScope.userLoginKey = null;
 	$location.path('/login');
 });
