'use strict';

/**
 * @ngdoc function
 * @name siufrontApp.controller:AcademichistoryCtrl
 * @description
 * # AcademichistoryCtrl
 * Controller of the siufrontApp
 */
 angular.module('siufrontApp')
 .controller('AcademichistoryCtrl', function ($scope, acHistoryService) {
 	var acHistPromise = acHistoryService.getAcademicHistory();

 	acHistPromise.then(function(result) {
 		$scope.acHistory = result;
 	}, function error(reason) {
 		console.log(reason.data.message);
 	});

 });
