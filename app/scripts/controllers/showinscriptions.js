'use strict';

/**
 * @ngdoc function
 * @name siufrontApp.controller:ShowinscriptionsCtrl
 * @description
 * # ShowinscriptionsCtrl
 * Controller of the siufrontApp
 */
 angular.module('siufrontApp')
 .controller('ShowinscriptionsCtrl', function ($scope, subjectService, examservice, $route) {
 	$scope.hasInscriptions = 0;
 	var subjectsPromise = subjectService.getSubjectsInformation();

 	subjectsPromise.then(function(result) {
 		$scope.subjectInscriptions = result.subjectInscriptions;
 		if(result.subjectInscriptions.length){
 			$scope.hasInscriptions = $scope.hasInscriptions + result.subjectInscriptions.length;
 		}
 	}, function error(reason) {
 		console.log(reason.data.message);
 	});

 	var examsPromise = examservice.getExamsInformation();

 	examsPromise.then(function(result) {
 		$scope.examInscriptions = result.examInscriptions;
 		if(result.examInscriptions.length){
 			$scope.hasInscriptions = $scope.hasInscriptions + result.examInscriptions.length;
 		}
 	}, function error(reason) {
 		console.log(reason.data.message);
 	});

 	$scope.removeInscription = function(subjectId, inscriptionId){
 		var removeInscription = subjectService.removeInscription(subjectId,
 			inscriptionId);
 		removeInscription.then(function(result){
 			Materialize.toast('La inscripción fue borrada', 4000);
 			$route.reload();
 		});
 	};

 	$scope.closePopup = function(element){
 		$('#' + element).closeModal();
 	};
 });
