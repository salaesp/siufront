'use strict';

/**
 * @ngdoc function
 * @name siufrontApp.controller:ExaminscriptionCtrl
 * @description
 * # ExaminscriptionCtrl
 * Controller of the siufrontApp
 */
 angular.module('siufrontApp')
 .controller('ExaminscriptionCtrl', function ($scope, $rootScope, examservice, subjectService, $route) {
 	var subjectServiceResponse = subjectService.getSubjectsInformation();

 	subjectServiceResponse.then(function(result) {
 		$scope.availableExams = result.availableExams;
 	}, function error(reason) {
 		console.log(reason.data.message);
 	});

 	$scope.addInscription = function(subjectId, variant) {
 		var examPromise = examservice.addInscription(subjectId, variant);

 		examPromise.then(function(result) {
 			Materialize.toast('Inscripción creada', 4000);
 			$route.reload();
 		}, function error(reason) {
 			console.log(reason.data.message);
 		});
 	};

 });
