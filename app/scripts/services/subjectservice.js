'use strict';

/**
 * @ngdoc service
 * @name siufrontApp.subjectService
 * @description
 * # subjectService
 * Factory in the siufrontApp.
 */
 angular.module('siufrontApp')
 .factory('subjectService',['Restangular','configuration', '$rootScope', function (Restangular,configuration, $rootScope) {
  return {
    getSubjectsInformation: function(){
      var academicHistoryUrl = configuration.siubackurl + 'academicunit/'+
      $rootScope.selectedAcademicUnit+'/career/'+ $rootScope.selectedCareer+
      '/careerplan/'+$rootScope.selectedCareerPlan+'/subject';

      var headerObj = { 'siuback-loginkey': $rootScope.userLoginKey};
      var params = {'regularities' : 'true', 'inscriptions' : 'true'};

      var restAcadamicHistory = Restangular.oneUrl('academicHistory', academicHistoryUrl);
      return restAcadamicHistory.get(params, headerObj);
    },
    addInscription: function(subject, variant){
      var addSuscriptionUrl = configuration.siubackurl + 'academicunit/'+
      $rootScope.selectedAcademicUnit+'/career/'+$rootScope.selectedCareer+
      '/careerplan/'+$rootScope.selectedCareerPlan+'/subject/'+subject;

      var headerObj = { 'siuback-loginkey': $rootScope.userLoginKey};
      var post = {'inscriptionAction':'SUBJECT_INSCRIPTION','variant': ''+variant+''};
      var restAddSuscription = Restangular.oneUrl('addSuscription', addSuscriptionUrl);
      return restAddSuscription.post('', post, {}, headerObj);
    },
    removeInscription: function(subject, inscription){
      var removeInscriptionUrl = configuration.siubackurl + 'academicunit/'+
      $rootScope.selectedAcademicUnit+'/career/'+$rootScope.selectedCareer+
      '/careerplan/'+$rootScope.selectedCareerPlan+'/subject/'+subject+
      '/inscription/'+inscription;

      var headerObj = { 'siuback-loginkey': $rootScope.userLoginKey };
      var restDelete = Restangular.oneUrl('removeInscription', removeInscriptionUrl);
      return restDelete.remove('',headerObj);
    }
  };
}]);
