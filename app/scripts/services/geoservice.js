'use strict';

/**
 * @ngdoc service
 * @name siufrontApp.geoService
 * @description
 * # geoService
 * Factory in the siufrontApp.
 */
 angular.module('siufrontApp')
 .factory('geoService',['Restangular','configuration', '$rootScope', function (Restangular, configuration, $rootScope) {
 	return {
 		putUserCoordinates: function (lat, lon) {
 			var headerObj = { 'siuback-loginkey': $rootScope.userLoginKey};
 			var putCoordinates = Restangular.oneUrl('putCoordinates', configuration.siubackurl + 'user/me');
 			putCoordinates.coordinates=lat+','+lon;
 			return putCoordinates.put({}, headerObj).then(function(){
 				var getNearby = Restangular.oneUrl('getNearby', configuration.siubackurl + 'user/me/nearby');
 				return getNearby.get({}, headerObj);
 			});
 		},

 		putUserCoordinatesWithStatus: function (lat, lon, status) {
 			var headerObj = { 'siuback-loginkey': $rootScope.userLoginKey};
 			var putCoordinates = Restangular.oneUrl('putCoordinates', configuration.siubackurl + 'user/me');
 			putCoordinates.coordinates=lat+','+lon;
 			putCoordinates.status=status;
 			return putCoordinates.put({}, headerObj);
 		}

 	};
 }]);
