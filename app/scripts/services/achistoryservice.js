'use strict';

/**
 * @ngdoc service
 * @name siufrontApp.acHistoryService
 * @description
 * # acHistoryService
 * Factory in the siufrontApp.
 */
 angular.module('siufrontApp')
 .factory('acHistoryService',['Restangular','configuration', '$rootScope', function (Restangular, configuration, $rootScope) {
 	return {
 		getUserCareers: function () {
 			var headerObj = { 'siuback-loginkey': $rootScope.userLoginKey};
 			var userLoginUrl = Restangular.oneUrl('userMe', configuration.siubackurl + 'user/me');
 			return userLoginUrl.get({}, headerObj);
 		},
 		getAcademicHistory: function(){
 			var academicHistoryUrl = configuration.siubackurl + 'academicunit/' +
 			$rootScope.selectedAcademicUnit + '/career/' + $rootScope.selectedCareer + 
 			'/careerplan/' + $rootScope.selectedCareerPlan + '/';
 			var headerObj = { 'siuback-loginkey': $rootScope.userLoginKey};
 			var restAcadamicHistory = Restangular.oneUrl('academicHistory', academicHistoryUrl);
 			return restAcadamicHistory.get({}, headerObj);
 		}
 	};
 }]);
