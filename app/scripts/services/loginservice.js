'use strict';

/**
 * @ngdoc service
 * @name siufrontApp.LoginService
 * @description
 * # LoginService
 * Factory in the siufrontApp.
 */
 angular.module('siufrontApp')
 .factory('LoginService', ['$base64', 'Restangular', 'configuration',  function ($base64, Restangular,configuration) {
 	return {
 		restLogin: function (loginInfo) {
 			var loginKey = 'Basic ' + $base64.encode(loginInfo.username + ':' + loginInfo.password);
 			var headerObj = { 'Authorization': loginKey };
 			var userLoginUrl = Restangular.oneUrl('userLoginUrl', configuration.siubackurl + 'user/login');
 			return userLoginUrl.get({}, headerObj);
 		}
 	};
 }]);
