'use strict';

/**
 * @ngdoc service
 * @name siufrontApp.examservice
 * @description
 * # examservice
 * Factory in the siufrontApp.
 */
 angular.module('siufrontApp')
 .factory('examservice',['Restangular','configuration', '$rootScope',  function (Restangular, configuration, $rootScope) {
  return {
    getExamsInformation: function(){
      var examsUrl =  configuration.siubackurl + 'academicunit/' +
      $rootScope.selectedAcademicUnit + '/career/' + $rootScope.selectedCareer + 
      '/careerplan/' + $rootScope.selectedCareerPlan + '/exam';

      var headerObj = { 'siuback-loginkey': $rootScope.userLoginKey};
      var params = {'inscriptions' : 'true'};

      var restAcadamicHistory = Restangular.oneUrl('exams', examsUrl);
      return restAcadamicHistory.get(params, headerObj);
    },
    addInscription: function(subject, variant, loginKey){
      var addSuscriptionUrl =  configuration.siubackurl + 'academicunit/' +
      $rootScope.selectedAcademicUnit + '/career/' + $rootScope.selectedCareer + 
      '/careerplan/' + $rootScope.selectedCareerPlan + '/subject/'+subject;

      var headerObj = { 'siuback-loginkey': $rootScope.userLoginKey};
      var post = {'inscriptionAction':'EXAM_INSCRIPTION','variant': ''+variant+''};
      var restAddSuscription = Restangular.oneUrl('addSuscription', addSuscriptionUrl);
      return restAddSuscription.post('', post, {}, headerObj);
    },
    removeInscription: function(subject, inscription, loginKey){
      var removeInscriptionUrl =  configuration.siubackurl + 'academicunit/' +
      $rootScope.selectedAcademicUnit + '/career/' + $rootScope.selectedCareer + 
      '/careerplan/' + $rootScope.selectedCareerPlan + '/exam/'+subject+
      '/inscription/'+inscription;

      var headerObj = { 'siuback-loginkey': $rootScope.userLoginKey};
      var restDelete = Restangular.oneUrl('removeInscription', removeInscriptionUrl);
      return restDelete.remove('',headerObj);
    }
  };
}]);
