'use strict';

/**
 * @ngdoc directive
 * @name siufrontApp.directive:ngPrepareModal
 * @description
 * # ngPrepareModal
 */
 angular.module('siufrontApp')
 .directive('ngPrepareModal', function ($timeout) {
 	return {
 		restrict: 'A',
 		link: function () {
 				$timeout(function() { 
 					$('.modal-trigger').leanModal();
 				},300);
 		}
 	};
 });
