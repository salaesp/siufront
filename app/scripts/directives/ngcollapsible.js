'use strict';

/**
 * @ngdoc directive
 * @name siufrontApp.directive:ngCollapsible
 * @description
 * # ngCollapsible
 */
 angular.module('siufrontApp')
 .directive('ngCollapsible', function ($timeout) {
 	return {
 		restrict: 'A',
 		link: function () {
 			$timeout(function() { 
 				jQuery('.collapsible').collapsible();
 			},300);
 		}
 	};
 });
